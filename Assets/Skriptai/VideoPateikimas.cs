﻿using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using System.Threading;
using System.Collections.Generic;
using System.Collections;

public class VideoPateikimas : MonoBehaviour
{
	public GameObject linkPrefab;
    public Text URL;
    public Text vietaTekstas;
    public Text infoText;
    private bool connected;
    public List<string> links = new List<string>();
	public Text [] linkTexts;
	public GameObject linkButton;
	public GameObject content;
	private int STEP = 50;
	//public float width = Screen.width;
	//public float height = Screen.height;

    DatabaseReference reference;
    DependencyStatus dependencyStat;
    //prisijunkite prie interneto!
    public IEnumerator ShowMessage(string message, float delay)
    {
        infoText.text = message;
        infoText.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
		gameObject.GetComponent<MeniuLogika> ().EitiVideo ();
        StartCoroutine(CheckConnection());
        infoText.gameObject.SetActive(false);
    }
	//public void IEnumerator delay(float time);

    void Start()
    {
        StartCoroutine(CheckConnection());

        dependencyStat = FirebaseApp.CheckDependencies();

        if (dependencyStat != DependencyStatus.Available)
        {
            FirebaseApp.FixDependenciesAsync().ContinueWith(task =>
                {
                    dependencyStat = FirebaseApp.CheckDependencies();
                    if (dependencyStat == DependencyStatus.Available)
                    {
                        Debug.Log("PAEJO");
                    }
                    else
                    {
                        Debug.Log("neeeePAEJO");
                    }
                });
        }
        else
        {
            Debug.Log("Prisijunge");
        }
    }

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Q)) {
			DuomenuSaugojimas.running = true;
			DuomenuSaugojimas.isPlaying = true;
			DuomenuSaugojimas.playingID = 0;
			DownloadLinks ();
		}
	}

    public void PushToServer(string vieta)
    {
        string key = reference.Child("vieta").Child(vieta).Push().Key;
		reference.Child("vieta").Child(vieta).Child(key).Child("hyperlink").SetValueAsync(URL.text);
        reference.Child("vieta").Child(vieta).Child(key).Child("approved").SetValueAsync(false);
    }

    public void PateiktiVideo()
    {	
        if (!connected)
        {
            StartCoroutine(ShowMessage("Prisijunkite prie interneto", 3));

        }
        else
        {
            PushToServer(vietaTekstas.text);
			StartCoroutine(ShowMessage("Nuoroda pateikta", 3));					
		}
    }

    public void DownloadLinks()
	{ 
		//IEnumerator ExecuteAfterTime(float time){
        FirebaseApp app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl("https://bite-ar.firebaseio.com");
        if (app.Options.DatabaseUrl != null) app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

        FirebaseDatabase.DefaultInstance
            .GetReference("vieta").Child(DuomenuSaugojimas.locationNames[DuomenuSaugojimas.playingID])
          .ValueChanged += (object sender2, ValueChangedEventArgs e2) => {
              
            if (e2.DatabaseError != null)
              {
                  Debug.LogError(e2.DatabaseError.Message);
                  return;
              }
              
              if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
              {
                Debug.Log("EUREKA!!");
				links.Clear();
                foreach (DataSnapshot ds in e2.Snapshot.Children)
                {
                    Debug.Log(ds.Child("approved").Value.ToString());
                    if (ds.Child("approved").Value.ToString() == "True")
                    {
                        links.Add("" + ds.Child("hyperlink").Value);
                    }
                }
					//Invoke("ListLinks(links)",2);
				StartCoroutine(ExecuteAfterTime(1));
				//ListLinks(links);
              }
          };
    }
	IEnumerator ExecuteAfterTime(float time){
		yield return new WaitForSeconds (time);	
		ListLinks(links);
	}


	public void ListLinks(List<string> links)
	{
		for (int i = 0; i < links.Count; i++)
		{
			
			GameObject child = (GameObject) Instantiate (linkButton, Vector3.zero, transform.rotation);
			//(linkButton, new Vector3 (0, 300 - STEP * i, 0), transform.rotation);
			child.GetComponent<link> ().URL = links [i];
			//Debug.Log (Screen.width);
			//Debug.Log (Screen.height);
			child.transform.SetParent (content.transform, true);
			//if(Screen.

			//child.transform.position = new Vector3 (390, 1000 - STEP * i, 0);
			//800*1280
			//child.transform.position = new Vector3 (400, 1000 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//1200*1920
			//child.transform.position = new Vector3 (600, 1500 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//800*1024
			//child.transform.position = new Vector3 (395, 850 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//600*1024
			//child.transform.position = new Vector3 (300, 850 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//480*800
			//child.transform.position = new Vector3 (240, 650 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//400*800
			//child.transform.position = new Vector3 (200, 650 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//640*960
			//child.transform.position = new Vector3 (320, 800 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//1440*2560
			//child.transform.position = new Vector3 (720, 2100 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//1600*2560
			//child.transform.position = new Vector3 (800, 2100 - child.GetComponent<RectTransform>().rect.height * i, 0);
			//1600*2560
			//Debug.Log(Screen.width);

		
			child.transform.position = new Vector3 (Screen.width/2, (Screen.height*4/5) - child.GetComponent<RectTransform>().rect.height * i, 0);



			child.GetComponentInChildren<Text>().text = links [i];
			//linkButton[i] = links[i];
			//Debug.Log (child.transform.position);
			child.transform.TransformPoint (Vector3.zero);


		}
		DuomenuSaugojimas.meniuLogika.EitiSarasas();

	}


    IEnumerator CheckConnection()
    {
        WWW www = new WWW("http://google.lt");
        yield return www;
        if (www.error != null)
        {
            connected = false;
        }
        else
        {
            connected = true;
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://bite-ar.firebaseio.com");
            // Get the root reference location of the database.
            reference = FirebaseDatabase.DefaultInstance.RootReference;
            //StartCoroutine(GetLinks("Čičinsko kalnas"));
        }
    }
}
