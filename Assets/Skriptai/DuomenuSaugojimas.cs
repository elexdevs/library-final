﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuomenuSaugojimas : MonoBehaviour
{
    public static int taskai;
    public static int[] atsakyti = {0, 0, 0, 0, 0, 0, 0};

    public static MeniuLogika meniuLogika;
    public static KlausimuLogika klausimuLogika;
    public static ARController kontroleris;
    public static VideoNuorodos nuorodos;
    public static Audio garsas;

    public static bool running = false;
    public static bool isPlaying = false;
    public static int playingID;

    public static Color mainColor = new Color(255, 255, 125);
    public static Color answeredColor = new Color(30, 255, 0);

    public static string[] locationNames = {
            "Čičinsko kalnas", "Mukolų akmenys", "Terpeikių ąžuolas", "Migonių piliakalnis",
        "Puntuko akmuo", "Karvės ola", "Dviragio ežeras"
        };

    static int versija = 1;

    public static void Issiusti()
    {
        PlayerPrefs.SetInt("Taskai", taskai);

        for (int i = 0; i < atsakyti.Length; i++)
        {
            PlayerPrefs.SetInt("A" + i, atsakyti[i]);
        }

        PlayerPrefs.Save();
    }

    public static void Parsisiusti()
    {
        if (versija != PlayerPrefs.GetInt("Versija", 0))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("Versija", versija);
        }

        taskai = PlayerPrefs.GetInt("Taskai", 0);

        for (int i = 0; i < atsakyti.Length; i++)
        {
            atsakyti[i] = PlayerPrefs.GetInt("A" + i, 0);
        }

    }
}
