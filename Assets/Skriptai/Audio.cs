﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    static AudioSource player;

    void Start()
    {
        DuomenuSaugojimas.garsas = this;
        player = Camera.main.GetComponent<AudioSource>();
    }

    float timer = 0;
    float maxTime = 0;
    bool timerActive = false;

    static Speaker currentSpeaker;

    void Update()
    {
        if (timerActive)
        {
            timer += Time.deltaTime;
        }

        if (timer > (maxTime + 0.5f))
        {
            DuomenuSaugojimas.klausimuLogika.ParengtiTeiginius(currentSpeaker.animationID);
            timerActive = false;
            DuomenuSaugojimas.isPlaying = false;
            maxTime = 0;
            timer = 0;
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Speaker")
                {
                    currentSpeaker = hit.collider.gameObject.GetComponent<Speaker>();
                    currentSpeaker.PlaySound();
                    maxTime = currentSpeaker.clip.length;
                    timerActive = true;
                }
				if (hit.collider.tag == "Logo")
				{
					DuomenuSaugojimas.kontroleris.StopAR();

					Application.OpenURL ("http://appvil.eu");
					//StartCoroutine(ExecuteAfterTime(1));
					//Application.backgroundLoadingPriority = ThreadPriority.High;
					//Destroy;
					Application.Quit();
					Debug.Log ("prius");
				}
            }
        }
#else
        if (Input.touchCount > 0)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Speaker")
                {
                    currentSpeaker = hit.collider.gameObject.GetComponent<Speaker>();
                    currentSpeaker.PlaySound();
                    maxTime = currentSpeaker.clip.length;
                     timerActive = true;
                 }
		if (hit.collider.tag == "Logo")
		{
		DuomenuSaugojimas.kontroleris.StopAR();

		Application.OpenURL ("http://appvil.eu");
		//StartCoroutine(ExecuteAfterTime(1));
		//Application.backgroundLoadingPriority = ThreadPriority.High;
		//Destroy;
		Application.Quit();
		Debug.Log ("prius");
		}
            }
        }
#endif
    }

    public static void PlaySound(AudioClip c)
    {
        if (!player.isPlaying)
        {
            player.PlayOneShot(c);
            DuomenuSaugojimas.isPlaying = true;
            DuomenuSaugojimas.playingID = currentSpeaker.animationID;
        }
    }

    public void StopSound()
    {
        player.Stop();
        timer = 0;
        maxTime = 0;
        timerActive = false;
        DuomenuSaugojimas.meniuLogika.infoPlokste.SetActive(false);
    }
}
