﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoNuorodos : MonoBehaviour
{
    public Text [] linkTexts;
	public GameObject linkButton;
	public GameObject content;

    public void ListLinks(List<string> links)
    {
        for (int i = 0; i < links.Count; i++)
        {
			GameObject child = Instantiate (linkButton);
			child.GetComponent<link> ().URL = links [i];
			child.transform.parent = content.transform;
            //linkTexts[i].text = links[i];

        }
        DuomenuSaugojimas.meniuLogika.EitiSarasas();
    }

    void Start()
    {
        DuomenuSaugojimas.nuorodos = this;
    }
}
