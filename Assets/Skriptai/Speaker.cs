﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speaker : MonoBehaviour
{
    [Header("Animacijos Garsas")]
    public AudioClip clip;
    [Header("Animacijos Klausimu ID")]
    public int animationID;
    [Header("Objekto Pavadinimas")]
    public string objectName;
    [Header("Isjungtas")]
    public Mesh offMesh;
    public Material offMaterial;

    bool isEnabled = true;

    void Start()
    {//jei teiginys atsakytas keisti spalva
        if (DuomenuSaugojimas.atsakyti [animationID] == 1)
        {
            ChangeToOff();
        }
    }

    void Update()
    {
        if (DuomenuSaugojimas.isPlaying && DuomenuSaugojimas.running)
        {
            if (DuomenuSaugojimas.playingID == animationID)
            {
                Enable();
            }
            else
            {
                Disable();
            }
        }
        else if (!DuomenuSaugojimas.isPlaying && DuomenuSaugojimas.running)
        {
            Enable();
        }
        else if (!DuomenuSaugojimas.running)
        {
            Disable();
        }
    }

    public void PlaySound()
    {
        Audio.PlaySound(clip);
        DuomenuSaugojimas.meniuLogika.DisplayInfo(objectName);
    }
	//keisti spalva
    public void ChangeToOff()
    {
        gameObject.GetComponent<MeshFilter>().mesh = offMesh;
        gameObject.GetComponent<Renderer>().material = offMaterial;
    }

    void Disable()
    {
        if (isEnabled)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            isEnabled = false;
        }
    }

    void Enable()
    {
        if (!isEnabled)
        {
            if (DuomenuSaugojimas.atsakyti [animationID] == 1)
            {
                ChangeToOff();
            }
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            gameObject.GetComponent<BoxCollider>().enabled = true;
            isEnabled = true;
        }
    }
}
