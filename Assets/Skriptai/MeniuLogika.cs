﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeniuLogika : MonoBehaviour
{
    [Header("Plokstes")]
    public GameObject meniuPlokste;
    public GameObject apiePlokste;
    public GameObject profilisPlokste;
    public GameObject klausimaiPlokste;
    public GameObject pagrindinePlokste;
    public GameObject videoPlokste;
    public GameObject sarasasPlokste;
    public GameObject iseitiPlokste;
    public GameObject infoPlokste;
	public GameObject content;

    [Header("Info Tekstas")]
    public Text infoTextStub;

    public static Text infoText;

    public void EitiApie()
    {
        CloseAll();
        apiePlokste.SetActive(true);
    }

    public void EitiProfilis()
    {
        CloseAll();
        profilisPlokste.SetActive(true);
    }

    public void EitiMeniu()
    {
        CloseAll();
        meniuPlokste.SetActive(true);
        if (DuomenuSaugojimas.garsas != null)
        {
            DuomenuSaugojimas.garsas.StopSound();
        }
        DuomenuSaugojimas.running = false;
    }

    public void EitiPradeti()
    {
        if (infoPlokste.activeSelf)
        {
            CloseAll();
            infoPlokste.SetActive(true);
            pagrindinePlokste.SetActive(true);
        }
        else
        {
            CloseAll();
            pagrindinePlokste.SetActive(true);
        }
        DuomenuSaugojimas.running = true;
    }

    public void EitiKlausimai()
    {
        CloseAll();
        klausimaiPlokste.SetActive(true);
        DuomenuSaugojimas.running = false;
		//saraso isvalymas
		for (int i = content.transform.childCount -1; i >= 0; --i){
			GameObject.Destroy (content.transform.GetChild(i).gameObject);		
		}
		content.transform.DetachChildren ();
    }

    public void EitiSarasas()
    {
        CloseAll();
        sarasasPlokste.SetActive(true);
    }

	public void EitiVideo()
    {
        //CloseAll();
		videoPlokste.SetActive(!videoPlokste.activeSelf);
    }

    public void EitiIseiti()
    {
        if (meniuPlokste.activeSelf && !iseitiPlokste.activeSelf)
        {
            iseitiPlokste.SetActive(true);
            DuomenuSaugojimas.Issiusti();
        }
    }

    public void Iseiti()
    {
		//Destroy();
       Application.Quit();
		Debug.Log ("");
    }
	public void OnDestroy(){
		Application.Quit();
	}
    public void DisplayInfo (string s)
    {
        infoText.text = s;
        infoPlokste.SetActive(true);
    }


    void CloseAll()
    {
        pagrindinePlokste.SetActive(false);
        videoPlokste.SetActive(false);
        klausimaiPlokste.SetActive(false);
        meniuPlokste.SetActive(false);
        profilisPlokste.SetActive(false);
        apiePlokste.SetActive(false);
        iseitiPlokste.SetActive(false);
        sarasasPlokste.SetActive(false);
        infoPlokste.SetActive(false);
    }

	public void StopAR()
	{
		DuomenuSaugojimas.kontroleris.StopAR ();
	}

    void Start()
    {
        EitiMeniu();
        DuomenuSaugojimas.kontroleris = GameObject.Find("AR Toolkit").GetComponent<ARController>();
        DuomenuSaugojimas.meniuLogika = this;
        infoText = infoTextStub;
    }
		

    void Update()
    {
        // ----- DEBUG -----
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (videoPlokste.activeSelf)
            {
                EitiProfilis();
            }
            else if (klausimaiPlokste.activeSelf)
            {
                EitiPradeti();
            }
            else if (meniuPlokste.activeSelf && !iseitiPlokste.activeSelf)
            {
                iseitiPlokste.SetActive(true);
                DuomenuSaugojimas.Issiusti();
            }
            else
            {
                EitiMeniu();
            }
        }

        if (Input.GetKeyDown(KeyCode.F1))
            DuomenuSaugojimas.klausimuLogika.ParengtiTeiginius(Random.Range(0, 7));
        // -----------------
    }

}
