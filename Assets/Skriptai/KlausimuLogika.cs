﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KlausimuLogika : MonoBehaviour
{
    [System.Serializable]
    public class Teiginiai { public string pirmas; public string antras; public string trecias; public int teisingas; }

    [Header("Teksto Laukai")]
    public Text pirmasTekstas;
    public Text antrasTekstas;
    public Text treciasTekstas;
    public Text uzdangaTekstas;

    [Header("Teiginiai")]
    public Teiginiai[] teiginiai;
    public GameObject[] mygtukai;

    [Header("Korys Objektas")]
    public GameObject korysPlokste;
    public Image korysProfilis;
    public Image korysAnimacijai;

    [Header("Korio Stadijos")]
    public Sprite[] korioStadijos;

    int dabartinisObjektas = 0;

    public void ParengtiTeiginius(int objektas)
    {
        if (DuomenuSaugojimas.atsakyti[objektas] == 1)
        {
            for (int i = 0; i < 3; i++)
                mygtukai[i].SetActive(false);

            dabartinisObjektas = objektas;
            DuomenuSaugojimas.meniuLogika.EitiKlausimai();
        }
        else
        {
            for (int i = 0; i < 3; i++)
                mygtukai[i].SetActive(true);
            uzdangaTekstas.gameObject.SetActive(false);
            pirmasTekstas.text = teiginiai[objektas].pirmas;
            antrasTekstas.text = teiginiai[objektas].antras;
            treciasTekstas.text = teiginiai[objektas].trecias;
            dabartinisObjektas = objektas;
            DuomenuSaugojimas.meniuLogika.EitiKlausimai();
        }
    }

    public void Atsakyti(int atsakymas)
    {
        if (atsakymas == teiginiai[dabartinisObjektas].teisingas)
        {
            // ATSAKYMAS TEISINGAS
            PridetiTaska();
            uzdangaTekstas.gameObject.SetActive(true);
            uzdangaTekstas.text = "ATSAKYMAS TEISINGAS";
            for (int i = 0; i < 3; i++)
                mygtukai[i].SetActive(false);
            DuomenuSaugojimas.atsakyti[dabartinisObjektas] = 1;
            DuomenuSaugojimas.Issiusti();
        }
        else
        {
            // ATSAKYMAS NETEISINGAS
            uzdangaTekstas.gameObject.SetActive(true);
            uzdangaTekstas.text = "ATSAKYMAS NETEISINGAS";
            for (int i = 0; i < 3; i++)
                mygtukai[i].SetActive(false);
        }
    }

    bool onTimerOn = false, offTimerOn = false;  float onTimer = 0, offTimer = 0; float maxTime = 1;

    public void PridetiTaska()
    {
        korysPlokste.SetActive(true);
        korysAnimacijai.sprite = korioStadijos[DuomenuSaugojimas.taskai];
        DuomenuSaugojimas.taskai++;
        if (DuomenuSaugojimas.taskai >= 7)
            DuomenuSaugojimas.taskai = 7;
        onTimerOn = true;
    }

    void Update()
    {
        if (onTimerOn)
            onTimer += Time.deltaTime;
        if (onTimer >= maxTime)
        {
            onTimerOn = false;
            korysAnimacijai.sprite = korioStadijos[DuomenuSaugojimas.taskai];
            korysProfilis.sprite = korioStadijos[DuomenuSaugojimas.taskai];
            offTimerOn = true;
            onTimer = 0;
        }

        if (offTimerOn)
            offTimer += Time.deltaTime;
        if (offTimer > maxTime)
        {
            offTimerOn = false;
            korysPlokste.SetActive(false);
            //DuomenuSaugojimas.meniuLogika.EitiPradeti();
            offTimer = 0;
        }

        // ----- DEBUG ------
        if (Input.GetKeyDown(KeyCode.A))
            if (!onTimerOn && !offTimerOn)
                PridetiTaska();
        // ------------------
    }

    void Start()
    {
        DuomenuSaugojimas.klausimuLogika = this;
        DuomenuSaugojimas.Parsisiusti();
        korysProfilis.sprite = korioStadijos[DuomenuSaugojimas.taskai];
    }

}
